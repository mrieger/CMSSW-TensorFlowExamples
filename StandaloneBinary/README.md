# `StandaloneBinary` example

```bash
# in your CMSSW src directory
git clone https://gitlab.cern.ch/mrieger/CMSSW-TensorFlowExamples.git TensorFlowExamples

scram b

../bin/slc7_amd64_gcc820/tf_standalone
```
