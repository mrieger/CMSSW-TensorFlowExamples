# coding: utf-8

"""
Initialization file for the GraphLoadingMT example module.
"""


__all__ = ["graphLoadingMT"]


import FWCore.ParameterSet.Config as cms


graphLoadingMT = cms.EDAnalyzer("GraphLoadingMT",
    graphPath=cms.string("graph.pb"),
)
