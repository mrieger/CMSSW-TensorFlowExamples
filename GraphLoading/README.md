# `GraphLoading` example

### Single-threaded example

```bash
# in your CMSSW src directory
git clone https://gitlab.cern.ch/mrieger/CMSSW-TensorFlowExamples.git TensorFlowExamples

scram b

cmsRun TensorFlowExamples/GraphLoading/test/graphLoading_cfg.py
```


### Multi-threaded example

```bash
# in your CMSSW src directory
git clone https://gitlab.cern.ch/mrieger/CMSSW-TensorFlowExamples.git TensorFlowExamples

scram b

cmsRun -n 2 TensorFlowExamples/GraphLoading/test/graphLoadingMT_cfg.py
```
