/*
 * Example module that shows how to load and evaluate a constant TensorFlow graph in a CMSSW plugin
 * within multiple threads. If you are aiming to use the TensorFlow interface in your personal
 * plugin (!), make sure to include the following lines in your /plugins/BuildFile.xml:
 *
 *     <use name="PhysicsTools/TensorFlow" />
 *
 * Important: If you are using the TensorFlow interface in a file in the /src/ or /interface/
 * directory of your module, make sure to create a (global) /BuildFile.xml containing (at least):
 *
 *     <use name="PhysicsTools/TensorFlow" />
 *     <export>
 *         <lib name="1" />
 *     </export>
 *
 * Author: Marcel Rieger <marcel.rieger@cern.ch>
 */

#include <memory>

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/MakerMacros.h"

// notice the include from "stream" instead of "one"
#include "FWCore/Framework/interface/stream/EDAnalyzer.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"

#include "PhysicsTools/TensorFlow/interface/TensorFlow.h"

// Declaration of the data structure that is hold by the edm::GlobalCache.
// In TensorFlow, the computational graph is stored in a stateless graph object which can be shared
// by multiple session instances which handle the initialization of variables related to the graph.
// Following this approach in CMSSW, a graph should be stored in a GlobalCache which can be accessed
// by sessions owned by multiple stream module copies. Instead of using only the plain graph, we
// make use of a cache struct that can be extended in the future if nedded. In addition, the graph
// is protected via std::atomic, which should not affect the performance as it is only accessed in
// the module constructor and not in the actual analyze/produce loop.
struct CacheData
{
    CacheData()
        : graphDef(nullptr)
    {
    }

    std::atomic<tensorflow::GraphDef*> graphDef;
};

class GraphLoadingMT : public edm::stream::EDAnalyzer<edm::GlobalCache<CacheData>>
{
public:
    explicit GraphLoadingMT(const edm::ParameterSet&, const CacheData*);
    ~GraphLoadingMT();

    // two additional static methods for handling the global cache
    static std::unique_ptr<CacheData> initializeGlobalCache(const edm::ParameterSet&);
    static void globalEndJob(const CacheData*);

private:
    void beginJob();
    void analyze(const edm::Event&, const edm::EventSetup&);
    void endJob();

    // per module copy, only the session is stored
    tensorflow::Session* session_;
};

std::unique_ptr<CacheData> GraphLoadingMT::initializeGlobalCache(const edm::ParameterSet& config)
{
    // this method is supposed to create, initialize and return a CacheData instance
    CacheData* cacheData = new CacheData();

    // load the graph def and save it
    std::string graphPath = config.getParameter<std::string>("graphPath");
    std::cout << "loading graph from " << graphPath << std::endl;
    cacheData->graphDef = tensorflow::loadGraphDef(graphPath);

    // set some global configs, such as the TF log level
    tensorflow::setLogging("0");

    return std::unique_ptr<CacheData>(cacheData);
}

void GraphLoadingMT::globalEndJob(const CacheData* cacheData)
{
    // reset the graphDef
    if (cacheData->graphDef != nullptr)
    {
        delete cacheData->graphDef;
    }
}

GraphLoadingMT::GraphLoadingMT(const edm::ParameterSet& config, const CacheData* cacheData)
    : session_(nullptr)
{
    // mount the graphDef stored in cacheData onto the session
    session_ = tensorflow::createSession(cacheData->graphDef);
}

GraphLoadingMT::~GraphLoadingMT()
{
}

void GraphLoadingMT::beginJob()
{
}

void GraphLoadingMT::endJob()
{
    // close the session
    tensorflow::closeSession(session_);
    session_ = nullptr;
}

void GraphLoadingMT::analyze(const edm::Event& event, const edm::EventSetup& setup)
{
    // define a tensor and fill it with range(10)
    tensorflow::Tensor input(tensorflow::DT_FLOAT, { 1, 10 });
    float* d = input.flat<float>().data();
    for (float i = 0; i < 10; i++, d++)
    {
        *d = i;
    }

    // define the output and run
    std::cout << "session.run" << std::endl;
    std::vector<tensorflow::Tensor> outputs;
    tensorflow::run(session_, { { "input", input } }, { "output" }, &outputs);

    // check and print the output
    std::cout << " -> " << outputs[0].matrix<float>()(0, 0) << std::endl << std::endl;
}

DEFINE_FWK_MODULE(GraphLoadingMT);
